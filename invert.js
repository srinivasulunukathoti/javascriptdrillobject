 // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
  
function invert(testObject) {
    // check it is object or not
    if (typeof testObject) {
        //Creating empty object to store
        let invertObject = {};
        for (const key in testObject) {
            //Swapping value and key.
              value = testObject[key];
              invertObject[value] = key;
        }
        return invertObject;
    } else {
        return "Not a Object";
    }
};
//exports the code
module.exports = invert;