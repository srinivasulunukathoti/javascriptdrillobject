   // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
   
function mapObject(testObject , callback) {
    // check it is object or not
    if (typeof testObject) {
       //Iterating testObject
        for (const key in testObject) {
             callback(key, testObject);
        }
        return testObject;
    } else {
        return "Not a Object"
    }
};
//exports the code
module.exports = mapObject;