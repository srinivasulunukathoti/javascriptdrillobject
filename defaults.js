// Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
 
function defaults(testObject ,defaultProps) {
    // check it' objects or not
    if (typeof testObject && typeof defaultProps) {
       //Iterating defaultProps
        for (const key in defaultProps) {
            // check the is present both or not
            if (!Object.keys(testObject).includes(key)) {
               testObject[key]=defaultProps[key];
            }
        }
        return testObject;
    } else {
        return "Not a Object";
    }
};
//exports the code
module.exports = defaults;