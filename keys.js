// Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.

function keys(testObject) {
    // check it is object or not
    if (typeof testObject) {
        //create empty array to store
        let keysArray = [];
        for (const key in testObject) {
            //pushing the keys.
            keysArray.push(key);
        }

        return keysArray;
    } else {
        return "Not a Object";
    };
};
//exports the code
module.exports = keys;