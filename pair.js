 // Convert an object into a list of [key, value] pairs.
   
function pair(testObject) {
    // check it is object or not
    if (typeof testObject) {
        let PairArrays = [];
        for (const key in testObject) {
            PairArrays.push([key , testObject[key]]);
        }
        return PairArrays;
    } else {
        return "Not a array"
    }
};
//exports the code
module.exports = pair;