const invert = require('../invert');
const testObject = require('../data');

try {
    const result = invert(testObject);
    console.log(result);
} catch (error) {
    console.error(error);
}