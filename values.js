// Return all of the values of the object's own properties.
    // Ignore functions
function values(testObject) {
    // check it is object or not
    if (typeof testObject) {
       let valuesArray = []; 
        for (const key in testObject) {
           valuesArray.push(testObject[key]);
        }
        return valuesArray;
    } else {
        return "Not a Object";
    }
};
//exports the code
module.exports = values;